package fit5042.tutex.repository.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yan Wang
 */

@Entity
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_CUSTOMERS, query = "select c from Customer c")
    ,
  @NamedQuery(name = Customer.GET_LAST_CUSTOMER, query = "select c from Customer c order by c.customer_id desc"),})
@XmlRootElement

public class Customer implements Serializable {

    public static final String GET_ALL_CUSTOMERS = "Customer.getAll";
    public static final String GET_LAST_CUSTOMER = "getLastCustomer";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int customer_id;
    private String customerName;
    private String address;
    private String industry;
    private int no_of_employee;
    private String ceo_name;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date register_date;
    private String website;
    private String phone_no;
    
    @OneToMany(mappedBy = "customers",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
    private List<Cus_Contact> ownContact ;
    
    @ManyToOne(cascade = { CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "id", nullable = false)
    private Staff staffs;
    
    
	public Staff getStaffs() {
		return staffs;
	}



	public void setStaffs(Staff staffs) {
		this.staffs = staffs;
	}



	public Customer() {
	}

    
    
	public Customer(int customer_id, String customerName, String address, String industry, int no_of_employee,
			String ceo_name, Date register_date, String website, String phone_no) {
		super();
		this.customer_id = customer_id;
		this.customerName = customerName;
		this.address = address;
		this.industry = industry;
		this.no_of_employee = no_of_employee;
		this.ceo_name = ceo_name;
		this.register_date = register_date;
		this.website = website;
		this.phone_no = phone_no;

	}




	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public int getNo_of_employee() {
		return no_of_employee;
	}

	public void setNo_of_employee(int no_of_employee) {
		this.no_of_employee = no_of_employee;
	}

	public String getCeo_name() {
		return ceo_name;
	}

	public void setCeo_name(String ceo_name) {
		this.ceo_name = ceo_name;
	}

	public Date getRegister_date() {
		return register_date;
	}

	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public List<Cus_Contact> getOwnContact() {
		return ownContact;
	}

	public void setOwnContact(List<Cus_Contact> ownContact) {
		this.ownContact = ownContact;
	}
    
}
