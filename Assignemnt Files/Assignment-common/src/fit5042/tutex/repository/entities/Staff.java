package fit5042.tutex.repository.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Yan Wang
 */
@Entity
public class Staff extends AppUser implements Serializable {


    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dobb;
    private int salary;

    @OneToMany(mappedBy = "staffs",fetch = FetchType.EAGER,cascade ={ CascadeType.REMOVE})
    private List<Customer> customers;
    

	public Staff() {
	}
    
	public Staff(int powerLevel, String firstName, String lastName, String password, String email) {
		super(powerLevel, firstName, lastName, password, email);
		this.dobb = dobb;
		this.salary = salary;
		this.customers = customers;
	}



	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}




	
	public Date getDobb() {
		return dobb;
	}
	public void setDobb(Date dobb) {
		this.dobb = dobb;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

	

}
