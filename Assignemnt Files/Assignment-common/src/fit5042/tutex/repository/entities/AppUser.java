 package fit5042.tutex.repository.entities;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
*
* @author Yan Wang
*/

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQueries({
    @NamedQuery(name = AppUser.GET_ALL_USERS, query = "select u from AppUser u")
    ,
  @NamedQuery(name = AppUser.GET_LAST_USER, query = "select u from AppUser u order by u.id desc"),})
public class AppUser implements Serializable {

    public static final String GET_ALL_USERS = "getAllUsers";
    public static final String GET_LAST_USER = "getLastUsers";
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    
    private int powerLevel;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    
	public AppUser() {
	}
	
	
	
	public AppUser(int powerLevel, String firstName, String lastName, String password, String email) {
		this.powerLevel = powerLevel;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getPowerLevel() {
		return powerLevel;
	}
	
	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}
