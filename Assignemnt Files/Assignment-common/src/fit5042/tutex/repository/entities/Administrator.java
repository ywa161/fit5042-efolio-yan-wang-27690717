package fit5042.tutex.repository.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
*
* @author Yan Wang
*/
@Entity
public class Administrator extends AppUser implements Serializable {


	private int phoneNo;
	private int officeAddress;
   
	
	public Administrator() {
		super();
	}
	public Administrator(int phoneNo, int officeAddress, int powerLevel, String firstName, String lastName, String password, String email) {
		super(powerLevel, firstName, lastName, password, email);
		this.phoneNo = phoneNo;
		this.officeAddress = officeAddress;
	}
	
	public int getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
	}
	public int getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(int officeAddress) {
		this.officeAddress = officeAddress;
	}
}