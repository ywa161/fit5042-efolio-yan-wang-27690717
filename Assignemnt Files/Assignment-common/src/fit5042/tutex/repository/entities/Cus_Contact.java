package fit5042.tutex.repository.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yan Wang
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Cus_Contact.GET_ALL_Contact, query = "select s from Cus_Contact s")
    ,
  @NamedQuery(name = Cus_Contact.GET_LAST_Contact, query = "select s from Cus_Contact s order by s.contact_id desc"),})
@XmlRootElement

public class Cus_Contact implements Serializable {

    public static final String GET_ALL_Contact = "getAllContact";
    public static final String GET_LAST_Contact = "getLastContact";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int contact_id;
    private String gender;
    private String name;
    private int mobile_phone;
    private int age;
    private String work_time;
    
    @ManyToOne(cascade = { CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customers;

    public Cus_Contact() {
    	
    }
    
	public Cus_Contact(int contact_id, String gender, String name, int mobile_phone, int age, String work_time,
			Customer customers) {
		super();
		this.contact_id = contact_id;
		this.gender = gender;
		this.name = name;
		this.mobile_phone = mobile_phone;
		this.age = age;
		this.work_time = work_time;
		this.customers = customers;
	}

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(int mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getWork_time() {
		return work_time;
	}

	public void setWork_time(String work_time) {
		this.work_time = work_time;
	}

	public Customer getCustomer() {
		return customers;
	}

	public void setCustomer(Customer customers) {
		this.customers = customers;
	}

}
