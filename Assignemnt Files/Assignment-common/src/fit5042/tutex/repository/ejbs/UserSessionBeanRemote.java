package fit5042.tutex.repository.ejbs;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;
import fit5042.tutex.repository.entities.*;

@Remote
public interface UserSessionBeanRemote {
	//public AppUser validateUser(String email, String password);
	
	public List<AppUser> getAllUsers();
	
	public List<Customer> getAllCustomers();
	
	public List<Cus_Contact> getAllContact();
	
	
	public void deleteStaffById(int id);
	
	public void deleteCustomerById(int id);
	
	public void deleteContactById(int id);
	
	public Customer selectCustomerById(int id);
	
	public Cus_Contact selectContactById(int id);
	
	public Staff selectStaffById(int id);
	
	public Administrator selectAdminById(int id);
	
	public void addCustomer(Customer c);
	
	public void addAdmin(Administrator a);
	
	public void addContact(Cus_Contact c);
	
	public void addStaff(Staff s);
	
	public void updateCustomer(Customer c);
	
	public void updateAdmin(Administrator a);
	
	public void updateStaff(Staff a); 
	
	public void updateContact(Cus_Contact c);
	
	
}
