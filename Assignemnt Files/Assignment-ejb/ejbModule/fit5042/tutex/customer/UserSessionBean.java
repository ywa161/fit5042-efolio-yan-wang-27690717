package fit5042.tutex.customer;

import javax.ejb.Stateless;

import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


@Stateless
public class UserSessionBean implements UserSessionBeanRemote {
	@PersistenceContext(name = "Assignment-ejbPU")
	private EntityManager entityManager;
	
	@Override
	public List<Customer> getAllCustomers(){
		return entityManager.createNamedQuery(Customer.GET_ALL_CUSTOMERS).getResultList();
	}
	

	
	@Override
	public List<AppUser> getAllUsers(){
		return entityManager.createNamedQuery(AppUser.GET_ALL_USERS).getResultList();
	}
	
	@Override
	public List<Cus_Contact> getAllContact(){
		return entityManager.createNamedQuery(Cus_Contact.GET_ALL_Contact).getResultList();
	}
	
	
	@Override
	public void deleteCustomerById(int id) {
		entityManager.remove(entityManager.find(Customer.class, id));
	}
	
	@Override
	public void deleteContactById(int id) {
		entityManager.remove(entityManager.find(Cus_Contact.class, id));
	}
	
	@Override
	public void deleteStaffById(int id) {
		entityManager.remove(entityManager.find(Staff.class, id));
	}
	
	
	
	@Override
	public Customer selectCustomerById(int id) {
		return entityManager.find(Customer.class, id);
	}
	
	@Override
	public Cus_Contact selectContactById(int id) {
		return entityManager.find(Cus_Contact.class, id);
	}
	
	@Override
	public Staff selectStaffById(int id) {
		return entityManager.find(Staff.class, id);
	}
	
	@Override
	public Administrator selectAdminById(int id) {
		return entityManager.find(Administrator.class, id);
	}
	
	@Override
	public void addCustomer(Customer c) {
		int getStaff = c.getStaffs().getId();
		Staff staffa = entityManager.getReference(Staff.class, getStaff);
		c.setStaffs(staffa);
		entityManager.persist(c);
	}
	
	@Override
	public void addAdmin(Administrator a) {
		Query query = entityManager.createNamedQuery(AppUser.GET_LAST_USER);
		query.setMaxResults(1);
		AppUser lastUser = (AppUser)query.getSingleResult();
		a.setId(lastUser.getId()+1);
		entityManager.persist(a);
	}
	
	@Override
	public void addContact(Cus_Contact c) {
		int getCustomer = c.getCustomer().getCustomer_id();
		Customer customera = entityManager.getReference(Customer.class, getCustomer);
		c.setCustomer(customera);
		entityManager.persist(c);
	}
	
	@Override
	public void addStaff(Staff s) {
		entityManager.persist(s);
	}
	
	@Override
	public void updateCustomer(Customer c) {
		Date date = new Date();
		c.setRegister_date(date);
		int getStaff = c.getStaffs().getId();
		Staff staffa = entityManager.getReference(Staff.class, getStaff);
		c.setStaffs(staffa);
		entityManager.merge(c);
	}
	
	@Override
	public void updateAdmin(Administrator a) {
		entityManager.merge(a);
	}
	
	@Override
	public void updateStaff(Staff a){
		entityManager.merge(a);
	}
	
	
	
	@Override
	public void updateContact(Cus_Contact c) {
		entityManager.merge(c);
	}
	
	
	
}