/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.Cus_Contact;
import fit5042.tutex.repository.entities.Customer;
import javax.faces.context.FacesContext;


@Named(value = "getAllContact")
@RequestScoped
public class GetAllContact {
	private Customer customer;
	
    @EJB
    private UserSessionBeanRemote userSB;
	
    /**
     * Creates a new instance of GetAllUsers
     */
    public GetAllContact() {
    	
    }
    
	@PostConstruct
	public void init() {
		customer = new Customer();
	}
    
    public List<Cus_Contact> getAllContact() throws Exception {
        return userSB.getAllContact();
    }

}
