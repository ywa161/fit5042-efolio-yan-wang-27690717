/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.AppUser;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Staff;


@Named(value = "login")
@SessionScoped
public class Login implements Serializable {

    @EJB
    private UserSessionBeanRemote userSB;
    private static final long serialVersionUID = -1231439157655757940L;
    private String username;
    private String password;
    AppUser appuser;
    
    public Login() {
    	
    }
    
	@PostConstruct
	public void init() {
		username ="";
		password ="";
		appuser = new AppUser();	
	}
    
	public int getUserId() {
		return appuser.getId();
	}
    
	public String getUsername() {
		return username;
	}
	
	public int getUserPower() {
		return appuser.getPowerLevel();
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String signinSystem() {
		List<AppUser> users = userSB.getAllUsers();
		for (int index = 0; index < users.size();index++) {
			AppUser user = users.get(index);
			String name = user.getFirstName();
			String word = user.getPassword();
			if (name.equals(username) && word.equals(password)) {
				if(user.getPowerLevel()== 1) {
					appuser = user;
					return "index";
				}else {
					appuser = user;
					return "indexStaff";
				}
			}
		}
		return "error";
	}
  
}
