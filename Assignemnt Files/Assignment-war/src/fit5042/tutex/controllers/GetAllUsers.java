/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.AppUser;
import fit5042.tutex.repository.entities.Staff;


@Named(value = "getAllUsers")
@RequestScoped
public class GetAllUsers {

    @EJB
    private UserSessionBeanRemote userSB;
	
    /**
     * Creates a new instance of GetAllUsers
     */
    public GetAllUsers() {
    	
    }


    
    public List<AppUser> getAllUsers () throws Exception {
        return userSB.getAllUsers();
    }
    
    public ArrayList<AppUser> getAllStaffs () throws Exception {
        List<AppUser> users = userSB.getAllUsers();
        ArrayList<AppUser> appUsers = new ArrayList<>();
        for (int index = 0; index<users.size();index++) {
        	AppUser appusera = users.get(index);
        	if (appusera.getPowerLevel() == 2) {
        		appUsers.add(appusera);
        	}
        }
        return appUsers;
    }
}
