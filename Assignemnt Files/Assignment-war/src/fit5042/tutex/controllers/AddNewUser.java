/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.AppUser;
import fit5042.tutex.repository.entities.Staff;


@Named(value = "addNewUser")
@RequestScoped
public class AddNewUser {

    @EJB
    private UserSessionBeanRemote userSB;
	
    private Staff staff;
    private int powerLevel;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private int salary;
    /**
     * Creates a new instance of GetAllUsers
     */
    
    
    public AddNewUser() {
    	
    }

    public UserSessionBeanRemote getUserSB() {
		return userSB;
	}

	public void setUserSB(UserSessionBeanRemote userSB) {
		this.userSB = userSB;
	}

	public int getPowerLevel() {
		return powerLevel;
	}

	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String userGenerator() {
    	staff = new Staff();
    	return "addUsers";
    }
    
    public Staff getStaff() {
    	return staff;
    }
    
    public String addNewStaff () throws Exception {
    	staff = new Staff();
    	staff.setPowerLevel(2);
    	Date date = new Date();
    	staff.setDobb(date);
    	staff.setFirstName(firstName);
    	staff.setLastName(lastName);
    	staff.setEmail(email);
    	staff.setSalary(salary);
    	staff.setPassword(password);
        userSB.addStaff(staff);
        return "Users";
    }
}
