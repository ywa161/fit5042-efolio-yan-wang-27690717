/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.Customer;


@Named(value = "getAllCustomers")
@RequestScoped
public class GetAllCustomers {

    @EJB
    private UserSessionBeanRemote userSB;
	
    /**
     * Creates a new instance of GetAllUsers
     */
    public GetAllCustomers() {
    	
    }

    
    public List<Customer> getAllCustomer () throws Exception {
        return userSB.getAllCustomers();
    }
    
}
