/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.servlet.http.HttpServletRequest;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.AppUser;
import fit5042.tutex.repository.entities.Cus_Contact;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Staff;
import javax.faces.context.FacesContext;


@Named(value = "addNewContact")
@SessionScoped
public class AddNewContact implements Serializable{

    @EJB
    private UserSessionBeanRemote userSB;
    
    private Cus_Contact contact;
    private static final long serialVersionUID = -1231439157655757940L;
    private int contact_id;
    private String gender;
    private String name;
    private int mobile_phone;
    private int age;
    private String work_time;
    Customer customer;
    
    /**
     * Creates a new instance of GetAllUsers
     */
	public String contactGenerator(Customer oldcustomer) {
    	return "addContact";
    }
	
    public String selectContact() {
		customer = new Customer();
		contact = new Cus_Contact();
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		int id = Integer.valueOf(request.getParameter("id"));
    	customer = userSB.selectCustomerById(id);
    	return "addContact";
    }
    
	
	@PostConstruct
	public void init() {
		customer = new Customer();
	}
	
	public String addNewContact () throws Exception {
		contact = new Cus_Contact();
		contact.setGender(gender);
		contact.setName(name);
		contact.setMobile_phone(mobile_phone);
		contact.setAge(age);
		contact.setWork_time(work_time);
		contact.setCustomer(customer);
        userSB.addContact(contact);
        return "Contact";
    }

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(int mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getWork_time() {
		return work_time;
	}

	public void setWork_time(String work_time) {
		this.work_time = work_time;
	}
}
