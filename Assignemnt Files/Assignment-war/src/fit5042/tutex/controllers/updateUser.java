/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.*;


@Named(value = "updateUser")
@RequestScoped
public class updateUser {

    @EJB
    private UserSessionBeanRemote userSB;
	
    private Administrator admin;
    private Administrator indexadmin;
    private Staff staff;
    private Staff indexstaff;
    /**
     * Creates a new instance of updateUser
     */
    public updateUser() {
    	
    }
    
    public int getStaffId() {
    	return admin.getId();
    }
    
    public int getStaffPowerLevel() {
    	return admin.getPowerLevel();
    }
    
    public Administrator returnAdministrator() {
    	return admin;
    }
    
    public Staff returnStaff() {
    	return staff;
    }
    
    @PostConstruct
    public void init() {
    	staff = new Staff();
        admin = new Administrator();
    }
    
    public String ShowAdminDetailsReadOnly(int a_id) {
        admin = userSB.selectAdminById(a_id);
        
        return "adminReadOnly";
    }
    
    public String ShowStaffDetailsReadOnly(int a_id) {
        staff = userSB.selectStaffById(a_id);
        
        return "staffReadOnly";
    }
    
    public String ShowStaffDetailsReadOnlyStaff(int a_id) {
        staff = userSB.selectStaffById(a_id);
        
        return "staffReadOnlyStaff";
    }
    
    public String ShowAdminDetails(int a_id) {
        admin = userSB.selectAdminById(a_id);
        indexadmin = admin;
        return "adminEdit";
    }
    
    public String ShowAdminDetailsSelf(int a_id) {
        admin = userSB.selectAdminById(a_id);
        indexadmin = admin;
        return "adminEditSelf";
    }
    
    public String ShowStaffDetails(int a_id) {
        staff = userSB.selectStaffById(a_id);
        return "staffEdit";
    }

    public String ShowStaffDetailsSelf(int a_id) {
        staff = userSB.selectStaffById(a_id);
        return "staffEditSelf";
    }
    
    public String updateStaff() {
        userSB.updateStaff(staff);
        return "Users";
    }
    
    public String updateStaffStaff() {
        userSB.updateStaff(staff);
        return "userStaff";
    }
    
    public String updateStaffSelf() {
        userSB.updateStaff(staff);
        return "indexStaff";
    }
    
    public String updateAdmin() {
    	
        userSB.updateAdmin(admin);
        return "Users";
    }
    
    public String updateAdminSelf() {
        userSB.updateAdmin(admin);
        return "index";
    }
    
    
    public String deleteStaffById(int userId) {

        userSB.deleteStaffById(userId);

        return "Users";
    }
    

}
