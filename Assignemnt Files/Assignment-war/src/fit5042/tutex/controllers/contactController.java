/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.Cus_Contact;
import fit5042.tutex.repository.entities.Customer;


@Named(value = "contectController")
@SessionScoped
public class contactController  implements Serializable {
	Customer customer;
    @EJB
    private UserSessionBeanRemote userSB;
    private static final long serialVersionUID = -1231439157655757940L;
    /**
     * Creates a new instance of GetAllUsers
     */
    public contactController() {
    	
    }
    
	@PostConstruct
	public void init() {
		customer = new Customer();
	}
    
	
	public Customer getCustomer() {
		return customer;
	}
	
    
    public String selectContact(int customerId) {
		customer = new Customer();
    	customer = userSB.selectCustomerById(customerId);
    	return "Contact";
    }
    
    public int getCustomerId() {
    	return customer.getCustomer_id();
    }
    
    public boolean authorize (int contactCustomer) {
    	if (contactCustomer == customer.getCustomer_id()) {
    		return true;
    	}else {
    		return false;
    	}
    }
    
    
}
