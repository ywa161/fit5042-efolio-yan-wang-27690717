/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.*;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

@Named(value = "updateContact")
@SessionScoped
public class UpdateContact  implements Serializable {

    @EJB
    private UserSessionBeanRemote userSB;
    private static final long serialVersionUID = -1231439157655757940L;
    private Cus_Contact contact;
    private int contact_id;
    private String gender;
    private String name;
    private int mobile_phone;
    private int age;
    private String work_time;
    private Customer customer;
    
    
    
    public int getContact_id() {
		return contact_id;
	}
    
    public Cus_Contact getContact() {
    	return contact;
    }

	@PostConstruct
	public void init() {
		contact = new Cus_Contact();
		customer = new Customer();
	}
	
	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getMobile_phone() {
		return mobile_phone;
	}


	public void setMobile_phone(int mobile_phone) {
		this.mobile_phone = mobile_phone;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getWork_time() {
		return work_time;
	}


	public void setWork_time(String work_time) {
		this.work_time = work_time;
	}


	/**
     * Creates a new instance of updateUser
     */
    
    public UpdateContact() {
    	
    }

    public String ShowContactDetailsReadOnly(int a_id) {
    	contact = userSB.selectContactById(a_id);
        
        return "ContactReadOnly";
    }
    
    public String ShowContactDetails(int a_id) {
    	contact = userSB.selectContactById(a_id);
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		int id = Integer.valueOf(request.getParameter("id"));
    	customer = userSB.selectCustomerById(id);
        return "ContactEdit";
    }
     
    public String UpdateContacts() {
    	contact.setCustomer(customer);
    	userSB.updateContact(contact);
        return "Contact";
    }
    
    public String deleteContactById(int userId) {

        userSB.deleteContactById(userId);

        return "Contact";
    }
    
}
