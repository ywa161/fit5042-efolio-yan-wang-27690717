/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.AppUser;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Staff;


@Named(value = "addNewCustomer")
@RequestScoped
public class AddNewCustomer {

    @EJB
    private UserSessionBeanRemote userSB;
	
    private Customer customer;
    private int customer_id;
    private String customerName;
    private String address;
    private String industry;
    private int no_of_employee;
    private String ceo_name;
    private Date register_date;
    private String website;
    private String phone_no;
    private int staff_id;
    
    public int getStaff_id() {
		return staff_id;
	}




	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}




	/**
     * Creates a new instance of GetAllUsers
     */
    
    
    public AddNewCustomer() {
    	
    }
    
    

    
    public int getCustomer_id() {
		return customer_id;
	}




	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}




	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public String getIndustry() {
		return industry;
	}




	public void setIndustry(String industry) {
		this.industry = industry;
	}




	public int getNo_of_employee() {
		return no_of_employee;
	}




	public void setNo_of_employee(int no_of_employee) {
		this.no_of_employee = no_of_employee;
	}




	public String getCeo_name() {
		return ceo_name;
	}




	public void setCeo_name(String ceo_name) {
		this.ceo_name = ceo_name;
	}




	public Date getRegister_date() {
		return register_date;
	}




	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}




	public String getWebsite() {
		return website;
	}




	public void setWebsite(String website) {
		this.website = website;
	}




	public String getPhone_no() {
		return phone_no;
	}




	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String customerGenerator() {
		customer = new Customer();
    	return "addCustomer";
    }

	public String customerGeneratorAdmin() {
		customer = new Customer();
    	return "addCustomerAdmin";
    }
	
	public String addNewCustomerAdmin () throws Exception {
    	customer = new Customer();
    	customer.setCustomerName(customerName);
    	Date date = new Date();
    	customer.setRegister_date(date);
    	customer.setAddress(address);
    	customer.setIndustry(industry);
    	customer.setNo_of_employee(no_of_employee);
    	customer.setCeo_name(ceo_name);
    	customer.setWebsite(website);
    	customer.setPhone_no(phone_no);
        Staff staff = userSB.selectStaffById(staff_id);
        customer.setStaffs(staff);
        userSB.addCustomer(customer);
        return "CustomerAdmin";
    }
	
	public String addNewCustomer (int id) throws Exception {
    	customer = new Customer();
    	customer.setCustomerName(customerName);
    	Date date = new Date();
    	customer.setRegister_date(date);
    	customer.setAddress(address);
    	customer.setIndustry(industry);
    	customer.setNo_of_employee(no_of_employee);
    	customer.setCeo_name(ceo_name);
    	customer.setWebsite(website);
    	customer.setPhone_no(phone_no);
        Staff staff = userSB.selectStaffById(id);
        customer.setStaffs(staff);
        userSB.addCustomer(customer);
        return "Customer";
    }
}
