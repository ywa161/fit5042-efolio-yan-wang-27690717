/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.Temporal;
import javax.enterprise.context.RequestScoped;
import fit5042.tutex.repository.ejbs.UserSessionBeanRemote;
import fit5042.tutex.repository.entities.*;


@Named(value = "updateCustomer")
@RequestScoped
public class UpdateCustomer {

    @EJB
    private UserSessionBeanRemote userSB;
    
    private Customer customer;
    private int customer_id;
    private String customerName;
    private String address;
    private String industry;
    private int no_of_employee;
    private String ceo_name;
    private Date register_date;
    private String website;
    private String phone_no;
    private int staff_id;
    
    
    public int getStaff_id() {
		return staff_id;
	}


	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}


	/**
     * Creates a new instance of updateUser
     */
    
    public UpdateCustomer() {
    	
    }


	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public int getCustomer_id() {
		return customer_id;
	}


	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getIndustry() {
		return industry;
	}


	public void setIndustry(String industry) {
		this.industry = industry;
	}


	public int getNo_of_employee() {
		return no_of_employee;
	}


	public void setNo_of_employee(int no_of_employee) {
		this.no_of_employee = no_of_employee;
	}


	public String getCeo_name() {
		return ceo_name;
	}


	@PostConstruct
	public void init() {
		customer = new Customer();
	}
	
	public void setCeo_name(String ceo_name) {
		this.ceo_name = ceo_name;
	}


	public Date getRegister_date() {
		return register_date;
	}


	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}


	public String getPhone_no() {
		return phone_no;
	}


	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
    public String ShowCustomerDetailsReadOnly(int a_id) {
        customer = userSB.selectCustomerById(a_id);
        
        return "CustomerReadOnly";
    }
    
    
    
    public String ShowCustomerDetails(int a_id) {
        customer = userSB.selectCustomerById(a_id);
        return "CustomerEdit";
    }
    
    public String ShowCustomerDetailsAdmin(int a_id) {
        customer = userSB.selectCustomerById(a_id);
        return "CustomerEditAdmin";
    }
    
    public String UpdateCustomers(int id) {
    	Staff staffs = userSB.selectStaffById(id);
    	customer.setStaffs(staffs);
    	userSB.updateCustomer(customer);
        return "Customer";
    }
    
    public String UpdateCustomersAdmin() {
    	Staff staffs = userSB.selectStaffById(staff_id);
    	customer.setStaffs(staffs);
    	userSB.updateCustomer(customer);
        return "CustomerAdmin";
    }
    
    public String deleteCustomerById(int userId) {
        userSB.deleteCustomerById(userId);
        return "Customer";
    }
    
    public String deleteCustomerByIdAdmin(int userId) {
        userSB.deleteCustomerById(userId);
        return "CustomerAdmin";
    }
    
}
